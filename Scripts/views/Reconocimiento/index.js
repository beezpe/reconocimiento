// Oculta la segunda pantalla de cada carta
document.getElementById("compromiso-2").style.display = "none";
document.getElementById("orientacion-2").style.display = "none";
document.getElementById("liderazgo-2").style.display = "none";

// Validacion para mostrar la segunda carta
function showDetail(card){ 
  if(card == "compromiso"){
    document.getElementById("compromiso-1").style.display = "none";
    document.getElementById("compromiso-2").style.display = "inline";
  } else if ( card == "orientacion"){
    document.getElementById("orientacion-1").style.display = "none";
    document.getElementById("orientacion-2").style.display = "inline";
  } else if( card == "liderazgo"){
    document.getElementById("liderazgo-1").style.display = "none";
    document.getElementById("liderazgo-2").style.display = "inline";
  }
}

