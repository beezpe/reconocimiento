

document.querySelector(".aplausos-button-2").onclick = function(){

	document.getElementsByClassName("modal-aplauso")[0].classList.add("mostrar")
}


const form_aplausos = document.getElementById("formAplausosContent");
const but_card = document.getElementById("btnAplaudir");
const cards = document.querySelectorAll(".custom-control-input");
const cards_content = document.querySelectorAll(".single-card-aplausos-2");

for (const option of cards) {
	option.addEventListener('click', function(event) {

		but_card.removeAttribute('disabled');
		form_aplausos.classList.remove('disabled');
		
		for (var i = 0; i < cards_content.length; i++) {
		    cards_content[i].classList.remove('active')
	  	}


		event.target.closest(".single-card-aplausos-2").classList.add("active")
		
	})
}



document.querySelector(".btnCloseAplaudir").onclick = function(){

	document.getElementsByClassName("modal-aplauso")[0].classList.remove("mostrar")
}